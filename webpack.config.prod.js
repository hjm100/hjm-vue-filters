const path = require('path')
module.exports = {
  entry: './filters/index.js',
  output: {
    path: path.resolve(__dirname, './dist'),
    publicPath: '/dist/',
    filename: 'main.js',
    library: 'filters',
    libraryTarget: 'umd'
  },
  mode: 'production',
  module: {
    rules: [
      {
        test: /\.js$/,
        use: ['babel-loader'],
        include: path.join(__dirname, './filters/'),
        exclude: /node_modules/
      }
    ]
  }
}
