# 更新日志
## 1.0.1

### 添加功能

1. 时间戳按照格式转时间
1. 时间戳获取星期
1. 货币单位转换
1. 根据数量级补充万
1. 数字补零（小于十的数字前面补零）

## 1.0.0

### 添加功能

1. 手机号格式化(中间四位用星号显示)
1. 格式化银行卡（4位一空格）
1. 千分位格式化
1. 超出长度用自定义文本显示(默认4位省略号显示)
1. 数字补零（小于十的数字前面补零）