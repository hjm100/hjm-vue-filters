module.exports = {
  extends: 'standard',
  rules: {
    'arrow-parens': [2, 'as-needed'],
    'space-before-function-paren': ['error', 'never']
  }
}
