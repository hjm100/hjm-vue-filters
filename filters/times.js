
/**
 * 注意事项
 * 时间戳： 10位或者13位 js获取时间为13位 php获取时间戳是10位，可以根据自己的需要来
 */
// 判断时间戳位数，输出13位时间戳
let switchTimestamp = timestamp => {
  if (timestamp.toString().length === 13) {
    return timestamp * 1
  } else {
    return timestamp * 1000
  }
}
/**
 * 时间戳格式化工具
 * @param {时间戳} timestamp
 * @param {格式} style
 */
export let timeFormate = (timestamp, style) => {
  let time = switchTimestamp(timestamp)
  // 首先判定sytle的格式
  let date = new Date(time)
  let year = date.getFullYear()
  let month = (date.getMonth() + 1).toString().padStart(2, 0)
  let day = date
    .getDate()
    .toString()
    .padStart(2, 0)
  let hour = date
    .getHours()
    .toString()
    .padStart(2, 0)
  let minute = date
    .getMinutes()
    .toString()
    .padStart(2, 0)
  let seconds = date
    .getSeconds()
    .toString()
    .padStart(2, 0)
  return style
    .replace('YYYY', year)
    .replace('MM', month)
    .replace('DD', day)
    .replace('HH', hour)
    .replace('mm', minute)
    .replace('ss', seconds)
}

/**
 * 时间戳转化星期
 * @param {时间戳} timestamp
 */

export let getWeek = timestamp => {
  // 首先判定sytle的格式
  let time = switchTimestamp(timestamp)
  let date = new Date(time)
  let WEEK = ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六']
  return WEEK[date.getDay()]
}
