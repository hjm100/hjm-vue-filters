/**
 * 货币单位转换
 * @param {货币数值} value
 * @param {单位} unit
 */
export let setMoney = (value, unit) => {
  return value + unit
}

/**
 * 根据数量级补充万
 * @param {数字} value
 * @param {量级} order
 * @param {单位} number
 */
export let addWan = (value, order, unit) => {
  if (value * 1 > order) {
    return Math.floor(value / order) + unit
  } else {
    return value
  }
}
