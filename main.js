import Vue from 'vue'
import * as filters from './filters/index'

Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})
// app setup
export default new Vue({
  el: '#app',
  data: {
    timestamp: 1553244907000, // 2019-03-22 16:55:07
    money: 10000, // 一万元
    wan: 1000000, // 一百万
    phone: '18612908099', // 手机号
    bank: '6217002430029882272', // 银行卡号
    name: '我爱鸿基梦',
    one: 0
  }
})
