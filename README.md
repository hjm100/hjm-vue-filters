# hjm-vue-filters

#### 介绍
1. vue过滤器封装实现，总结日常项目常用过滤器，能够支持单个过滤器引入使用

#### 项目架构

```md
.
|—— dist                      // 打包生成文件
|—— filters                   // 过滤器源码
|   |—— unit.js               // 单位转化
|   |—— times.js              // 时间过滤
|   └── index.js              // 入口文件
|—— main.js                   // 开发demo入口
|—— index.js                  // 开发demo文件    
|—— .babelrc
|—— .eslintrc.js
|—— .gitignore                // 限制git上传文件
|—— .babelrc
|—— package.json              // 本地npm管理文件  
|—— JEST.md                   // 单元测试说明(暂时没有集成)
|—— CHANGELOG.md              // 更新日志
|—— README.md                 // 项目说明
|—— webpack.config.js         // 打包脚本
.
```
#### 使用教程

1. 安装npm包： npm i hjm-vue-filters

1. 在项目中使用

```js

//在main.js注入

// 1. 引入全部方法
import * as filters from 'hjm-vue-filters'
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

// 2. 单个方法引入
import { timeFormate, getWeek } from 'hjm-vue-filters'
Vue.filter('timeFormate', timeFormate)
Vue.filter('getWeek', getWeek)

// 在项目中使用过滤器

<p>{{ timestamp|timeFormate('YYYY-MM-DD HH:mm:ss') }}</p>

```

#### 开发说明

1. filters目录里新建index.js文件为过滤器汇总文件
1. 编写开发日常过滤器，必须按照规范
1. 单个过滤器，单个方法导出，分类别区分过滤器文件

```js
exports.formatMoney = (value) => {
  if (value) {
    value = Number(value);
    return '￥ ' + value.toFixed(2);
  }
};

exports.statusName = (val) => {
  let statusName = ['已取消','未付款','已付款']
  return statusName[val];
};

```
1. 效果测试与查看
1. main.js里注册filter

```js
import * as filters from './filters/index.js'
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

```

1. index.html中测试

```html

<p>{{ timestamp|timeFormate('YYYY-MM-DD HH:mm:ss') }}</p>

```

#### 运行项目

1. npm run dev 运行项目demo查看测试效果
2. npm run build 打包过滤器文件，可以在main.js中引入查看效果

#### 方法总结

##### 时间过滤

| 方法 | 参数 | 备注 |
| :-: | :-: | :-: |
| timeFormate | (timestamp:时间戳,style:格式：'YYYY-MM-DD HH:mm:ss') | 时间戳格式化工具 |
| getWeek | timestamp:时间戳 | 时间戳转化星期 |

##### 单位添加

| 方法 | 参数 | 备注 |
| :-: | :-: | :-: |
| setMoney | (value:货币数值, unit:单位) | 货币单位转换|
| addWan | (value:数字, order:量级, unit:单位) | 根据数量级补充万 |

##### 字符格式化

| 方法 | 参数 | 备注 |
| :-: | :-: | :-: |
| formatPhone | (phone:手机号) | 手机号格式化（中间四位用星号显示）|
| formatBank | (bank:银行卡号) | 格式化银行卡（4位一空格）|
| toThousands | (number:数字) | 千分位格式化|
| omitString | (str:操作字符, n:目标位数, text:拼接的字符串) | 超出长度用自定义文本显示(默认4位省略号显示) |
| add0 | (num:数字) | 数字补零（小于十的数字前面补零）|